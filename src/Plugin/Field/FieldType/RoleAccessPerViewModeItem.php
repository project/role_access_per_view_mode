<?php

namespace Drupal\role_access_per_view_mode\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin implementation of the 'role_access_per_view_mode' field type.
 *
 * @FieldType(
 *   id = "role_access_per_view_mode",
 *   label = @Translation("Role access per view mode config"),
 *   description = @Translation("This field stores entity view mode access per role."),
 *   default_widget = "role_access_per_view_mode_widget",
 *   default_formatter = "role_access_per_view_mode",
 * )
 */
class RoleAccessPerViewModeItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['settings'] = MapDataDefinition::create()
      ->setLabel(t('Role access per view mode'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'settings' => [
          'type' => 'blob',
          'serialize' => TRUE,
          'not null' => TRUE,
        ],
      ],
    ];
  }

}
