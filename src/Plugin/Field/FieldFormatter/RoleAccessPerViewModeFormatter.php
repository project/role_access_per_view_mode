<?php

namespace Drupal\role_access_per_view_mode\Plugin\Field\FieldFormatter;

use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Plugin extends the 'text_default' formatter.
 *
 * @FieldFormatter(
 *   id = "role_access_per_view_mode",
 *   label = @Translation("Selected view mode name as text"),
 *   field_types = {
 *     "role_access_per_view_mode"
 *   }
 * )
 */
class RoleAccessPerViewModeFormatter extends TextDefaultFormatter {}
