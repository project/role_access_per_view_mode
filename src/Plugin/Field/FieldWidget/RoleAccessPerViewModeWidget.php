<?php

namespace Drupal\role_access_per_view_mode\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Widget for Role Access Per View Mode field.
 *
 * @FieldWidget(
 *  id = "role_access_per_view_mode_widget",
 *  label = @Translation("Select list"),
 *  field_types = {
 *    "role_access_per_view_mode"
 *  }
 * )
 */
class RoleAccessPerViewModeWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * List of available view modes.
   *
   * @var array
   */
  protected $viewModes = [];

  /**
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entitydisplayRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $entity_type = $field_definition->getTargetEntityTypeId();
    $bundle = $field_definition->getTargetBundle();
    $this->entitydisplayRepository = $entity_display_repository;

    // Get all view modes for the current bundle.
    $view_modes = $this->entitydisplayRepository->getViewModeOptionsByBundle($entity_type, $bundle);

    $this->viewModes = $view_modes;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $values = $items[$delta]->getValue();

    if (empty($values['settings'])) {
      foreach (user_roles() as $role_id => $role) {
        foreach ($this->viewModes as $view_mode_id => $view_mode) {
          $values['settings'][$role_id][$view_mode_id] = 1;
        }
      }
    }

    $rapvm_form = role_access_per_view_mode_options($this->viewModes, $form, $values['settings']);
    $element += $rapvm_form['role_access_per_view_mode'];

    return $element;
  }

}
