#Role access per view mode

Access control for node entities per view mode. Allows site administrators the
ability to restrict access by content type or by individual node.

## Installation

Download (preferably using Composer) and enable the module using the usual
Drupal methods. There are no dependencies.

## Usage

### Per content type

On each content type's "Edit" page, select the roles that can access each view
mode in the "Role access per view mode" configuration area.

### Per node

For each content type you would like to restrict access per node, from the
"Manage fields" page add a field of type "Role access per view mode config". It
is suggested that this field not be displayed on any view mode, as this is only
configuration.

Then, configure access per node on each node's "Edit" page.
